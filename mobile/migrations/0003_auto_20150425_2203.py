# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mobile', '0002_mobile_condition'),
    ]

    operations = [
        migrations.RenameField(
            model_name='condition',
            old_name='type',
            new_name='name',
        ),
    ]
