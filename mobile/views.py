import json, requests
from django.shortcuts import get_object_or_404, render
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.views import generic

from .models import Mobile


class IndexView(generic.ListView):
    template_name = 'mobile/index.html'
    context_object_name = 'latest_mobile_list'

    def get_queryset(self):
        """Return the last five published mobiles."""
        return Mobile.objects.order_by('-pub_date')[:5]


class DetailView(generic.DetailView):
    model = Mobile
    template_name = 'mobile/detail.html'
def search(request):
	search_query = '*' + request.GET.get('q') + '*'
	data = {
    "query": {
        "query_string": { "query": search_query }
    	}
	}
	response = requests.post('http://127.0.0.1:9200/dubimobi_index/mobile/_search', data=json.dumps(data))
	#response_ex = [res['_source']['name'] for res in response.json()['hits']['hits'] ]
	search_result_ids = [res['_id'] for res in response.json()['hits']['hits'] ]
	search_result_data = Mobile.objects.filter(id__in=search_result_ids)
	return render(request, 'mobile/search.html', {'search_result_data': search_result_data})
	#print response.json()


#class SearchView(generic.DetailView):
#    model = Mobile
#    template_name = 'mobile/search.html'


#class ResultsView(generic.DetailView):
#   model = Mobile
#    template_name = 'mobile/results.html'