#Ref: http://pixabay.com/en/blog/posts/django-search-with-elasticsearch-47/
data = {
    "settings": {
        "number_of_shards": 4,
        "number_of_replicas": 1
    },
    "mappings": {
        "gudlet": {
            "properties": {
                "name": { "type": "string", "boost": 4 },
                "operatingsystem": { "type": "string", "boost": 2 }
            }
        }
    }
}

import json, requests
response = requests.put('http://127.0.0.1:9200/dubimobi_index/', data=json.dumps(data))
print response.text
