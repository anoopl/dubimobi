# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mobile', '0005_auto_20150429_1739'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mobile',
            name='photo',
            field=models.ImageField(upload_to=b'mobile/static/mobile/media'),
        ),
    ]
