from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    # Examples:
    # url(r'^$', 'gudlet.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^', include('mobile.urls')),
    url(r'^mobile/', include('mobile.urls', namespace="mobile")),
    url(r'^admin/', include(admin.site.urls)),
]
