from django.contrib import admin
from mobile.models import Mobile, Condition, Operatingsystem

# Register your models here.
class MobileAdmin(admin.ModelAdmin):
	fields = ['pub_date', 'name', 'operatingsystem', 'condition', 'price', 'photo']
	list_display = ('name', 'price', 'price', 'photo','pub_date', 'was_published_recently')
	list_filter = ['pub_date']
	search_fields = ['name']
# Register your models here.
admin.site.register(Condition)
admin.site.register(Operatingsystem)
admin.site.register(Mobile, MobileAdmin)
