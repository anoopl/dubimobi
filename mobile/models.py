import datetime
from django.utils import timezone
from django.db import models

# Create your models here.
class Condition(models.Model):
	 name = models.CharField(max_length=15)
	 def __unicode__(self):
		return self.name
class Operatingsystem(models.Model):
	name = models.CharField(max_length=15)
	version = models.CharField(max_length=15)
	def __unicode__(self):
		return self.name
class  Mobile(models.Model):
	name = models.CharField(max_length=15)
	price = models.FloatField(default=0.0)
	operatingsystem = models.ForeignKey(Operatingsystem)
	condition = models.ForeignKey(Condition)
	photo = models.ImageField(upload_to='media/mobile')
	pub_date = models.DateTimeField('date published')
	def was_published_recently(self):
		return self.pub_date >= timezone.now() - datetime.timedelta(days=1)
	def __unicode__(self): #Change to __str(self) on Python 3
		return self.name



#class Seller(models.Model):
#   name = models.CharField(max_length=15)
#	shop_id =
#	conatct_phone =
#	contact_email =
#
#	pub_date = models.DateTimeField('date published')
#	def __unicode__(self):
#		return self.name
#class Gudlet(models.Model):
#	name = models.CharField(max_length=15)
#	pub_date = models.DateTimeField('date published')
#	actual_price = models.FloatField(default=0.0)
#	offer_price = models.FloatField(default=0.0)
#	category = models.ForeignKey(Category)
#	available_at_shops = models.ForeignKey(Shop)
#	def was_published_recently(self):
#		return self.pub_date >= timezone.now() - datetime.timedelta(days=1)
#	def __unicode__(self):
#		return self.name