# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('mobile', '0003_auto_20150425_2203'),
    ]

    operations = [
        migrations.AddField(
            model_name='mobile',
            name='photo',
            field=models.ImageField(default=datetime.datetime(2015, 4, 29, 16, 44, 33, 224554, tzinfo=utc), upload_to=b'mobile'),
            preserve_default=False,
        ),
    ]
