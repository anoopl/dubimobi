# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mobile', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='mobile',
            name='condition',
            field=models.ForeignKey(default=1, to='mobile.Condition'),
            preserve_default=False,
        ),
    ]
