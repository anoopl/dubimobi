# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mobile', '0004_mobile_photo'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mobile',
            name='photo',
            field=models.ImageField(upload_to=b'mobile/media'),
        ),
    ]
