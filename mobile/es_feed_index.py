
# run by
#python manage.py shell
#>>> execfile('let/es_feed_index.py')
#future ref:
#http://www.cotellese.net/2007/09/27/running-external-scripts-against-django-models/
import json, requests
from mobile.models import Mobile 

data = ''
for p in Mobile.objects.all():
    data += '{"index": {"_id": "%s"}}\n' % p.pk
    data += json.dumps({
        "name": p.name
    })+'\n'
response = requests.put('http://127.0.0.1:9200/dubimobi_index/mobile/_bulk', data=data)
print response.text
